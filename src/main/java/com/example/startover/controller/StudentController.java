package com.example.startover.controller;

import com.example.startover.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/hello")
    public String hello() {

        return "Hello from Spring";
    }
    @GetMapping("/user")
    public String hello2() {

        return "Hello from Spring";
    }

    @GetMapping("/admin")
    public String hello3() {

        return "Hello from Spring";
    }



}
