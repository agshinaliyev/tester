package com.example.startover.service;


import com.example.startover.model.Student;
import com.example.startover.repository.StudentRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

//    private final StudentRepository studentRepository;
//
//
//    public List<Student> getAllBy() {
//
//        return studentRepository.findAll(ageGreaterThan(20));
//
//    }
//
//    public List<Student> getAllByName() {
//
//        return studentRepository.findAll(nameLike("Vali"));
//
//    }
//
//    public List<Student> getAllByCriteria(List<SearchCriteria> dto) {
//
//        StudentSpecification studentSpecification = new StudentSpecification();
//        dto.forEach(searchCriteria -> studentSpecification.add(searchCriteria));
//        return studentRepository.findAll(studentSpecification);
//    }
//
//
//    public Specification<Student> ageGreaterThan(int age) {
//
//        return new Specification<Student>() {
//            @Override
//            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
//                return criteriaBuilder.greaterThan(root.get(Student.Fields.age), age);
//            }
//        };
//
//    }
//
//    public Specification<Student> ageGreater(int age) {
//
//        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThan(root.get(Student.Fields.age), age);
//
//    }
//
//    public Specification<Student> nameLike(String name) {
//
//        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(Student.Fields.name), name + "%");
//
//    }

}
