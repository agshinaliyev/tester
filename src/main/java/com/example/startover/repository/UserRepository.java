package com.example.startover.repository;

import com.example.startover.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {




}
