package com.example.startover;

import com.example.startover.model.Authority;
import com.example.startover.model.Student;
import com.example.startover.model.User;
import com.example.startover.model.UserAuthority;
import com.example.startover.repository.AuthorityRepository;
import com.example.startover.repository.StudentRepository;
import com.example.startover.repository.UserRepository;
import com.example.startover.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

import static com.example.startover.model.UserAuthority.ADMIN;
import static com.example.startover.model.UserAuthority.USER;

@SpringBootApplication
@RequiredArgsConstructor
public class StartoverApplication implements CommandLineRunner {


    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;


    public static void main(String[] args) {
        SpringApplication.run(StartoverApplication.class, args);


    }


    @Override
    public void run(String... args) throws Exception {


        Authority user = Authority.builder()
                .authority(USER)
                .build();

        Authority admin = Authority.builder()
                .authority(ADMIN)
                .build();
        authorityRepository.save(user);
        authorityRepository.save(admin);


        User user1 = User.builder()
                .username("user")
                .password("password")
                .authorities(List.of(user))
                .build();
        User admin1 = User.builder()
                .username("admin")
                .password("password")
                .authorities(List.of(admin))
                .build();

        userRepository.save(user1);
        userRepository.save(admin1);

//        service.getAllByName().forEach(System.out::println);

//        for (int i = 0; i < 50; i++) {
//
//            Student student = Student.builder()
//                    .name("Ali" + i)
//                    .surname("Mammadov"+ i)
//                    .age(18)
//                    .gpa(55.5+ i)
//                    .address("Baku").build();
//            studentRepository.save(student);
//
//        }
//
//        for (int i = 50; i < 100; i++) {
//
//            Student student = Student.builder()
//                    .name("Vali"+ i)
//                    .surname("Valiyev"+ i)
//                    .age(get(20,30))
//                    .gpa(5.5+ i)
//                    .address("Baku").build();
//            studentRepository.save(student);
//
//        }


//
//        System.out.println(studentService.getStudentAgeMoreThan(5));
//
//        System.out.println(studentService.getStudentByName("Nobody"));

        //JDBC
//        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5455/postgres", "postgres", "password");
//        Statement statement = connection.createStatement();
//        ResultSet resultSet = statement.executeQuery("select * from student ");
//        while (resultSet.next()){
//            System.out.println(resultSet.getInt(1));
//            System.out.println(resultSet.getString(2));
//            System.out.println(resultSet.getString(3));
//            System.out.println(resultSet.getString(4));
//
//
//        }

//
//        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnitName");
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        entityManager.getTransaction().begin();
//        List<Student> resultList = entityManager.createNativeQuery("select * from student", Student.class).getResultList();
//        System.out.println(resultList);
//        entityManager.getTransaction().commit();
//        entityManager.close();

//        List<Integer> evenNumbers =new ArrayList<>();
//        List<Integer> numbers= Arrays.asList(1,2,3,4,5,6,7);
//
//
//        for (Integer num : numbers){
//
//            if(num %2 == 0){
//
//                evenNumbers.add(num);
//            }
//
//        }
//
//        List<String> names =Arrays.asList("Blah","Blah","Blah");
//        List<String > namesToUpperCase = new ArrayList<>();
//
//        for (String name : names){
//
//            namesToUpperCase.add(name.toUpperCase());
//
//
//        }
//        System.out.println(namesToUpperCase);
//
//
//
//
    }
    public int get(int min,int max){

        return (int)((Math.random()* (max-min) +min));

    }
}
