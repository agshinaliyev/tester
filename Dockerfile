FROM openjdk:17-alpine
COPY build/libs/startover-0.0.1-SNAPSHOT.jar /startover-app/
CMD ["java","-jar","/startover-app/startover-0.0.1-SNAPSHOT.jar"]
